from datetime import datetime
import pytz
import json
import ruamel.yaml as yaml

def from_json_str(s : str) -> object:
    return json.loads(s)

def get_utc_now() -> datetime:
    
    naive = datetime.now()
    
    local = pytz.timezone('Europe/Helsinki')
    
    local_dt = local.localize(naive, is_dst=True)
    
    utc_dt = local_dt.astimezone(pytz.utc)
    
    return utc_dt

def parse_utc_datetime(s : str) -> datetime:
    return datetime.strptime(s, "%Y-%m-%dT%H:%M:%S.%fZ").replace(tzinfo=pytz.utc)

def print_utc_datetime(dt : datetime) -> str:
    return dt.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

def load_hypercube(filename : str) -> object:

    with open(filename, 'r') as f:
        return yaml.safe_load(f)

