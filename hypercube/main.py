
from typing import Callable

from hypercube.api import HypercubeApi

def run_evaluation_in_loop(
    api : HypercubeApi, 
    evaluate_strategy_f : Callable, 
    n_evaluations : int =-1):

    i = 0
    while (n_evaluations > 0 and i < n_evaluations):
        i += 1
        ask_msg = api.ask()
        tell_msg = api.tell(ask_msg, evaluate_strategy_f)
        api.post_tell(tell_msg)
