
import requests

from hypercube import utils

class HypercubeApi(object):

    def __init__(self, hypercube, api_hostname):

        print(hypercube)

        self.api_endpoint = f"http://{api_hostname}/api/hypercubes/{hypercube['id']}"
        
        self.strategy_ask_endpoint  = self.api_endpoint + "/strategy/ask"
        self.strategy_tell_endpoint = self.api_endpoint + "/strategy/tell"
        
        self.headers = {
            'Content-Type': 'application/json'
        }

        res = requests.post(
            self.api_endpoint,
            json=hypercube,
            headers=self.headers
        )

        print(f"POST {self.api_endpoint}: {res}")

    def ask(self):

        res = requests.get(
            self.strategy_ask_endpoint,
            headers=self.headers)

        print(f"GET {self.strategy_ask_endpoint}: {res}")

        ask_msg = res.json()

        return ask_msg

    def evaluate_ask(self, ask, evaluate_strategy_f):

        evaluation_started_at = utils.get_utc_now()

        asked_at = utils.parse_utc_datetime(ask['asked_at'])
        queue_time_in_s = (evaluation_started_at - asked_at).total_seconds()
        
        print(f"Consuming new ASK request: {ask}")

        if 'strategy' not in ask:
            print("Field 'strategy' is missing in request!")
            return None
                
        evaluation = evaluate_strategy_f(ask['strategy'])
        
        evaluation_finished_at = utils.get_utc_now()
        evaluation_time_in_s = (evaluation_finished_at - evaluation_started_at).total_seconds()
        
        tell = ask
        tell['strategy_evaluation'] = evaluation
        tell['strategy_evaluation_loss'] = evaluation['loss']
        tell['evaluation_started_at'] = utils.print_utc_datetime(evaluation_started_at)
        tell['evaluation_finished_at'] = utils.print_utc_datetime(evaluation_finished_at)
        tell['evaluation_time_in_s'] = evaluation_time_in_s
        tell['queue_time_in_s'] = queue_time_in_s

        print(f"Publishing new TELL response: {tell}")
        
        return tell

    def tell(self, ask_msg, evaluate_strategy_f):

        tell_msg = self.evaluate_ask(ask_msg, evaluate_strategy_f)

        return tell_msg

    def post_tell(self, tell_msg):

        res = requests.post(
            self.strategy_tell_endpoint,
            json=tell_msg,
            headers=self.headers)

        print(f"POST {self.strategy_tell_endpoint}: {res}")
