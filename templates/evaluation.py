
import numpy as np

from time import sleep

def evaluate_strategy(strategy):

    evaluation = {
      'loss': np.random.rand()
    }
    
    # Simulate non-trivial computation
    sleep(3)
    
    return evaluation
