#!/usr/bin/env python3

from evaluation import evaluate_strategy

import requests
import os
import ruamel.yaml as yaml
from datetime import datetime
import pytz
import json

def from_json_str(s):
    return json.loads(s)

def get_utc_now():
    
    naive = datetime.now()
    
    local = pytz.timezone('Europe/Helsinki')
    
    local_dt = local.localize(naive, is_dst=True)
    
    utc_dt = local_dt.astimezone(pytz.utc)
    
    return utc_dt

def parse_utc_datetime(s):
    return datetime.strptime(s, "%Y-%m-%dT%H:%M:%S.%fZ").replace(tzinfo=pytz.utc)

def print_utc_datetime(dt):
    return dt.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

def tell_on_ask(ask):

    evaluation_started_at = get_utc_now()

    asked_at = parse_utc_datetime(ask['asked_at'])
    queue_time_in_s = (evaluation_started_at - asked_at).total_seconds()
    
    print(f"Consuming new ASK request: {ask}")

    if 'strategy' not in ask:
        print("Field 'strategy' is missing in request!")
        return None
            
    evaluation = evaluate_strategy(ask['strategy'])
    
    evaluation_finished_at = get_utc_now()
    evaluation_time_in_s = (evaluation_finished_at - evaluation_started_at).total_seconds()
    
    tell = ask
    tell['strategy_evaluation'] = evaluation
    tell['strategy_evaluation_loss'] = evaluation['loss']
    tell['evaluation_started_at'] = print_utc_datetime(evaluation_started_at)
    tell['evaluation_finished_at'] = print_utc_datetime(evaluation_finished_at)
    tell['evaluation_time_in_s'] = evaluation_time_in_s
    tell['queue_time_in_s'] = queue_time_in_s

    print(f"Publishing new TELL response: {tell}")
    
    return tell
    
def main():

    hypercube = yaml.safe_load(open('hypercube.yaml', 'r'))

    print(hypercube)

    api_endpoint = f"http://{os.environ['API_HOST']}/api/hypercubes/{hypercube['id']}"

    headers = {'Content-Type': 'application/json'}

    res = requests.post(api_endpoint, json=hypercube, headers=headers)

    print("add new hypercube:", res)

    while True:

        print("requesting new ask")

        res = requests.get(api_endpoint + "/strategy/ask", headers=headers)
        
        print(res)

        ask = res.json()

        print("new ask:", ask)
        
        tell = tell_on_ask(ask)
        
        requests.post(api_endpoint + "/strategy/tell", json=tell, headers=headers)
    

if __name__ == "__main__":
    main()
    
