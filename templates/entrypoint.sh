#!/bin/bash

set -ea

. env.sh

PYTHONPATH=${PWD}:${PYTHONPATH}

"$@"
