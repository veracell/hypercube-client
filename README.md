# hypercube-client

# Create new project
```
$ bin/hcloud projects create --project-dir projects/mytest --hypercube-id mytest

copy: projects/mytest <- templates/requirements.txt
copy: projects/mytest <- templates/Dockerfile
copy: projects/mytest <- templates/evaluation.py
copy: projects/mytest <- templates/hypercube.yaml
copy: projects/mytest <- templates/hc-requirements.txt
copy: projects/mytest <- templates/env.sh
copy: projects/mytest <- templates/entrypoint.sh
copy: projects/mytest <- templates/hypercube_worker.py
Project with hypercube_id=mytest created
```
To modify the project to your needs, just edit the following files:
```
requirements.txt
evaluation.py
hypercube.yaml
```

# Dryrun the project
After adjusting the project directory to your needs, start the dryrun by calling (it is assumed you have hypercube manager running in localhost)
```
bin/hcloud projects dryrun --project-id projects/mytest --api-hostname localhost
```

# Deploy the project to cloud
First you need to build the project
```
bin/hcloud projects build --project-dir projects/mytest
```
After this step, you should now see an image of the built project:
```
$ bin/hcloud images list
NAME
...
gcr.io/veracell-ai/hc-worker-mytest
...
```
To actually deploy this image, call
```
bin/hcloud images deploy --image-id gcr.io/veracell-ai/hc-worker-mytest --hypercube-id mytest --cluster-name hypercube-workers
```
It is assumed you have cluster `hypercube-workers` created. To check, call
```
bin/hcloud clusters list
```
If the cluster is not listed, create one by calling:
```
bin/hcloud clusters create --cluster-name hypercube-workers
```