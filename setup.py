from setuptools import setup
#from distutils.core import setup

setup(name='hypercube',
      version='1.0',
      packages=['hypercube'],
      install_requires=[
          'requests',
          'pytz',
          'ruamel.yaml'
      ]
)
